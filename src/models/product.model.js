const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * @swagger
 * definitions:
 *  Product:
 *    type: object
 *    properties:
 *      name:
 *        type: string
 *      category:
 *        type: string
 *      brand:
 *        type: string
 *      weight:
 *        type: number
 *      supplier:
 *        type: string
 */
let ProductSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'name is required.'],
    },
    category: {
      type: String,
      required: [true, 'category is required.'],
    },
    brand: {
      type: String,
      required: [true, 'brand is required.'],
    },
    weight: {
      type: Number,
      required: false,
    },
    supplier: {
      type: Schema.Types.ObjectId,
    },
  },
  {
    timestamps: true,
  }
);

let Product = mongoose.model('product', ProductSchema);

module.exports = Product;
