const constants = {
  EXCHANGE_PRODUCTS: 'product-catalog',
  SERVICE_NAME: 'product-catalog-management',
};

module.exports = constants;
