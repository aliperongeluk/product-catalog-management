const jwt = require('jsonwebtoken');
const rp = require('request-promise');
const express = require('express');
const routes = express.Router();

const events = require('../environment/constants/events');
const Product = require('../models/product.model');
const messageSender = require('../../src/eventhandlers/message.sender');
const secret = require('../../src/environment/config/definitlynotsecret.config');

/**
 * @swagger
 * /api/product-catalog-management/products:
 *    get:
 *     tags:
 *       - Products
 *     description: Retrieving all products.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *        description: All events are returned.
 *        schema:
 *          type: array
 *          items:
 *            $ref: '#/definitions/Product'
 *       400:
 *          description: Something went wrong.
 */
routes.get('/', (req, res) => {
  Product.find({})
    .then(p => {
      res.status(200).json(p);
      console.log(p);
    })
    .catch(error => {
      console.log(error);
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

/**
 * @swagger
 * /api/product-catalog-management/products/{id}:
 *    get:
 *     tags:
 *       - Products
 *     description: Retrieving all products.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *          type: string
 *         required: true
 *         description: String ID of the product.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *        description: All products are returned.
 *        schema:
 *          $ref: '#/definitions/Product'
 *       400:
 *          description: Something went wrong.
 */
routes.get('/:id', (req, res) => {
  const id = req.params.id;

  Product.findById(id)
    .then(p => res.status(200).json(p))
    .catch(error => {
      console.log(error);
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

/**
 * @swagger
 * /api/product-catalog-management/products:
 *    post:
 *     tags:
 *       - Products
 *     parameters:
 *       - in: header
 *         name: token
 *         schema:
 *          type: string
 *         required: true
 *       - in: body
 *         name: product
 *         description: Product object
 *         required: true
 *         type: string
 *         schema:
 *           $ref: '#/definitions/Product'
 *     description: Creating new product in product catalog database.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *        description: Product is saved and returned.
 *        schema:
 *          $ref: '#/definitions/Product'
 *       400:
 *          description: Something went wrong.
 *       401:
 *          description: No permission.
 *       404:
 *          description: User does not exist.
 */
routes.post('/', (req, res) => {
  const token = req.headers.token;
  const payload = jwt.decode(token.substr(7, token.length - 1));
  console.log(payload);
  console.log(req.headers);

  if (!payload) {
    res.status(404).json({
      error: 'User does not have permission to create products.',
    });
  }

  // roleId 0 = CUSTOMER (default rights)
  // roleId 1 = STAFF
  // roleId 2 = TRUSTED_SUPPLIER
  if (payload.user.roleId === 1 || payload.user.roleId === 2) {
    const product = new Product(req.body);
    product
      .save()
      .then(() => {
        messageSender.publish(events.PRODUCT_CREATED, product);
        res.status(200).send(product);
      })
      .catch(error => {
        console.log(error);
        res.status(400).json({ error: 'Something went wrong.' });
      });
  } else {
    res.status(401).json({
      error: 'User has insufficient permissions to create products.',
    });
  }
});

/**
 * @swagger
 * /api/product-catalog-management/products/{id}:
 *    delete:
 *     tags:
 *       - Products
 *     description: Delete product with given ID from database.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *          type: string
 *         required: true
 *         description: String ID of the product.
 *     responses:
 *       200:
 *        description: Product is deleted and returned.
 *        schema:
 *          $ref: '#/definitions/Product'
 *       400:
 *          description: Something went wrong.
 */
routes.delete('/:id', (req, res) => {
  const payload = jwt.decode(req.headers.token);
  if (!payload) {
    res.status(404).json({
      error: 'User does not have permission to create products..',
    });
  }
  const id = req.params.id;

  Product.findByIdAndDelete(id)
    .then(p => res.status(200).json(p))
    .catch(error => {
      console.log(error);
      res.status(400).json({ error: 'Something went wrong.' });
    });
});

module.exports = routes;
