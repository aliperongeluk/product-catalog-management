FROM node:10.13.0

WORKDIR /home/docktor/compositions/product-catalog-management

COPY package*.json ./

RUN npm install
RUN npm install pm2 -g

COPY . .

EXPOSE 7000

CMD ["pm2-runtime", "server.js"]
